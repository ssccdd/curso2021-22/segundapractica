﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Segunda Práctica
## Resolución con monitores

Para la solución de la práctica se utilizará como herramienta de concurrencia el desarrollo de monitores. Hay que tener presenta a la hora de implementar la solución que Java no dispone de esta herramienta. Para ello el alumno podrá utilizar cualquier utilidad de concurrencia para diseñar la clase que representa al monitor de la solución teórica. En la implementación del monitor se deberán respetar las características que presenta el monitor que se ha presentado en teoría.

## Problema a resolver
Para un restaurante tenemos dos tipos de clientes: clientes **estándar** y **premium**. Como los clientes premium tienen una tarjetan de fidelización, por la que pagan una cuota y solicitan platos más caros, el restaurante ha decidido dar prioridad a los clientes premium.

- Los clientes tienen priorizada la entrada al restaurante. Los clientes pueden llegar en un mismo momento al restaurante (cada cliente tarda en llegar un tiempo entre 0 y 10 segundos). Simulamos la generación de clientes de forma aleatoria y uniforme  (la mitad premium y la otra mitad estándar) y un espacio del restaurante para 10 personas. Entran mientras haya huecos, pero cuando se llena deben esperar en una cola. Cuando un cliente sale entra otro, pero siempre que haya un hueco libre entra un cliente premium primero, aunque el estándar lleve más rato esperando. Habrá que establecer algún criterio para que los clientes estándar no esperen de forma indefinida.

- Cuando los clientes están en el restaurante piden varios platos de diferente precio. Los clientes piden entre 3-5 platos. Los premium piden de un precio entre 10-40 € y los clientes estándar entre 5-20 €. Todos se sientan en mesas individuales y también piden y pagan individualmente.

- La cocina atiende los platos, no en orden de pedida, sino preparando el de precio más alto para que los clientes premium esperen menos. El tiempo que se tarda en preparar un plato estará simulado y será de un segundo.

- Cuando un plato de un cliente ha sido preparado, el cliente se lo puede comer. Para simplificar supondremos que el camarelo lo ha servido en su mesa. El cliente tarda entre 2-5 segundos en comerlo.

- Cuando acaba con el último plato, el cliente se marcha.

Para la solución hay que tener presente:

- Diseñar el monitor que representa al restaurante.
- Diseñar los procesos necesarios, incluido el proceso principal.
- Definir todas las constantes necesarias.

Para la implementación se tendrá que mostrar la siguiente información:

- El momento que un cliente llega al restaurante.
- El momento en el que el cliente entra en el restaurante.
- Los platos que ha pedido el cliente y el precio de los mismos.
- El momento en que abandona el restaurante.
- Recaudación del restaurante.

